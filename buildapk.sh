#!/bin/sh

# Android 
export ANDROID_HOME=/home/mathieu/Android/Sdk
export PATH=$ANDROID_HOME/platform-tools:$PATH
export PATH=$ANDROID_HOME/tools:$PATH
#export PATH=$ANDROID_HOME/build-tools/29.0.2:$PATH
export GRADLE_HOME=/opt/gradle/gradle-5.5
export PATH=${GRADLE_HOME}/bin:${PATH}


ionic cordova build android --prod --release 
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore msha.keystore -storepass cartel2000 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk msha
rm platforms/android/app/build/outputs/apk/release/msha.apk
zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk platforms/android/app/build/outputs/apk/release/msha.apk
cp platforms/android/app/build/outputs/apk/release/msha.apk .
