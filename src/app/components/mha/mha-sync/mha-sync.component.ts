import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mha-sync',
  templateUrl: './mha-sync.component.html',
  styleUrls: ['./mha-sync.component.scss'],
})
export class MhaSyncComponent implements OnInit {

  @Input() hasModification: boolean;
  constructor() { }

  ngOnInit() {}

}
