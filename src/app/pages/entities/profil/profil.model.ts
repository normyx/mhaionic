import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { Spent } from '../spent/spent.model';
import { Task } from '../task/task.model';
import { TaskProject } from '../task-project/task-project.model';
import { TodoList } from '../todo-list/todo-list.model';
import { Wallet } from '../wallet/wallet.model';
import { Workspace } from '../workspace/workspace.model';

export class Profil implements BaseEntity {
    constructor(
        public id?: number,
        public displayName?: string,
        public lastUpdate?: any,
        public userLogin?: string,
        public userId?: number,
        public profilDataId?: number,
        public spents?: Spent[],
        public tasks?: Task[],
        public taskProjects?: TaskProject[],
        public todoLists?: TodoList[],
        public wallets?: Wallet[],
        public workspaces?: Workspace[],
        public syncStatus?: SyncStatus
    ) {
    }
}
