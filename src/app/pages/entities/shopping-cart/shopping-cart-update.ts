import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ShoppingCart } from './shopping-cart.model';
import { ShoppingCartService } from './shopping-cart.service';
import { ShoppingCatalogCart, ShoppingCatalogCartService } from '../shopping-catalog-cart';
import { Workspace, WorkspaceService } from '../workspace';
import { Profil, ProfilService } from '../profil';

@Component({
    selector: 'page-shopping-cart-update',
    templateUrl: 'shopping-cart-update.html'
})
export class ShoppingCartUpdatePage implements OnInit {

    shoppingCart: ShoppingCart;
    shoppingCatalogCarts: ShoppingCatalogCart[];
    workspaces: Workspace[];
    profils: Profil[];
    lastUpdateDp: any;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        lastUpdate: [null, [Validators.required]],
          shoppingCatalogCartId: [null, []],
          workspaceId: [null, [Validators.required]],
          owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private shoppingCatalogCartService: ShoppingCatalogCartService,
        private workspaceService: WorkspaceService,
        private profilService: ProfilService,
        private shoppingCartService: ShoppingCartService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.shoppingCatalogCartService.query()
            .subscribe(data => { this.shoppingCatalogCarts = data.body; }, (error) => this.onError(error));
        this.workspaceService.query()
            .subscribe(data => { this.workspaces = data.body; }, (error) => this.onError(error));
        this.profilService.query()
            .subscribe(data => { this.profils = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.shoppingCart = response.data;
            this.isNew = this.shoppingCart.id === null || this.shoppingCart.id === undefined;
        });
    }

    updateForm(shoppingCart: ShoppingCart) {
        this.form.patchValue({
            id: shoppingCart.id,
            label: shoppingCart.label,
            lastUpdate: shoppingCart.lastUpdate,
            shoppingCatalogCartId: shoppingCart.shoppingCatalogCartId,
            workspaceId: shoppingCart.workspaceId,
            owners: shoppingCart.owners,
        });
    }

    save() {
        this.isSaving = true;
        const shoppingCart = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.shoppingCartService.update(shoppingCart));
        } else {
            this.subscribeToSaveResponse(this.shoppingCartService.create(shoppingCart));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ShoppingCart>>) {
        result.subscribe((res: HttpResponse<ShoppingCart>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `ShoppingCart ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/shopping-cart');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): ShoppingCart {
        return {
            ...new ShoppingCart(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            lastUpdate: this.form.get(['lastUpdate']).value,
            shoppingCatalogCartId: this.form.get(['shoppingCatalogCartId']).value,
            workspaceId: this.form.get(['workspaceId']).value,
            owners: this.form.get(['owners']).value,
        };
    }

    compareShoppingCatalogCart(first: ShoppingCatalogCart, second: ShoppingCatalogCart): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackShoppingCatalogCartById(index: number, item: ShoppingCatalogCart) {
        return item.id;
    }
    compareWorkspace(first: Workspace, second: Workspace): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackWorkspaceById(index: number, item: Workspace) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }
}
