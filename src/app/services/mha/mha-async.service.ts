import { Injectable } from '@angular/core';
import { MhaActiveProfilService } from './mha-active-profil.service/mha-active-profil.service';
import * as moment from 'moment'; 
import { LocalStorage } from 'ngx-webstorage';
@Injectable({ providedIn: 'root' })
export class MhaAsyncService {

    @LocalStorage('lastUpdate')
    lastUpdate: moment.Moment;

    constructor(
        protected profilService: MhaActiveProfilService) {
    }

    public async refresh(refreshAll:boolean) {
        await this.profilService.commitChanges();
        if (refreshAll || !this.lastUpdate) {
            await this.clear();
            this.lastUpdate = null;
        }
        await this.profilService.loadDataFromServer(true, this.lastUpdate);
        this.lastUpdate = moment();
    }

    public async clear() {
        await this.profilService.clear();
    }

}