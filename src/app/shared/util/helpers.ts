import { StatusBar } from '@ionic-native/status-bar/ngx';

export function setStatusBarBackgroundColor(statusBar: StatusBar, part: string) : void {
    switch (part) {
        case 'task' : {
            statusBar.backgroundColorByHexString('#d41243');
            break;
        }
        case 'workspace' :{
            statusBar.backgroundColorByHexString('#3880ff');
            break;
        }
        case 'wallet' :{
            statusBar.backgroundColorByHexString('#f47835');
            break;
        }
        case 'home' :{
            statusBar.backgroundColorByHexString('#00aedb');
            break;
        }
        case 'todo' :{
            statusBar.backgroundColorByHexString('#a200ff');
            break;
        }
        case 'shopping' :{
            statusBar.backgroundColorByHexString('#8ec127');
            break;
        }
        default: {
            statusBar.backgroundColorByName("white");
        }
    }
}